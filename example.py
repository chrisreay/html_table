from datetime import date
from html_table import DateCol, LinkCol, NumCol, StrCol, HtmlTable

class Person(object):
    def __init__(self, id, name, age, anniversay):
        self.id = id
        self.name = name
        self.age = age
        self.anniversay = anniversay

    def to_table_row(self):
        return [f'/person/{self.id}', self.name, self.age, self.anniversay]

if __name__ == "__main__":
    cols = [LinkCol("Id"), StrCol("Name"), NumCol("Age"), DateCol("Anniversary")]
    people = [
        Person(1, 'Alice', 23, date(2021, 2, 3)), 
        Person(2, 'Bob', 34, date(2022, 3, 4)), 
        Person(3, 'Carol', 45, date(2023, 4, 5))
    ]
    rows = [person.to_table_row() for person in people]
    table = HtmlTable(cols, rows, css_class='ms-table ms-striped ms-bordered')
    print(table.generate())