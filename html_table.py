from datetime import date
from attr import attrs, attrib

@attrs
class Col(object):
    name = attrib()
    items = attrib(factory=list)

    '''
    def __init__(self, name):
        self.name = name
        self.items = []
    '''

    def data(self, i):
        return f'<td>{self.items[i]}</td>'

    def header(self):
        return f'<th>{self.name}</th>'

class DateCol(Col):
    def __init__(self, name, format='%Y-%m-%d'):
        super().__init__(name)
        self.format = format

    def data(self, i):
        return f'<td>{self.items[i].strftime(self.format)}</td>'

class LinkCol(Col):
    def data(self, i):
        return f'<td><a href="{self.items[i]}">{self.name}</a></td>'

class NumCol(Col):
    def data(self, i):
        return f'<td style="text-align: right">{self.items[i]}</td>'

    def header(self):
        return f'<th style="text-align: right">{self.name}</th>'

class StrCol(Col):
    pass

@attrs
class HtmlTable(object):
    columns = attrib()
    rows = attrib()
    css_class = attrib(default='')

    def itemise(self):
        for row in self.rows:
            for i, item in enumerate(row):
                self.columns[i].items.append(item)

    def header(self):
        if self.css_class:
            return f'<table class="{self.css_class}">'
        else:
            return '<table>'

    def generate(self):
        self.itemise()
        table = []
        table.append(self.header())
        # Header
        table.append('<tr>')
        for item in self.columns:
            table.append(item.header())
        table.append('</tr>')
        # Rows
        for i in range(len(self.rows)):
            table.append('<tr>')
            for j in range(len(self.columns)):
                table.append(self.columns[j].data(i))
            table.append('</tr>')
        #
        table.append('</table>')
        return '\n'.join(table)
        